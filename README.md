# Experiment 13+ #

## 13+.1 Denumire ##

Unipolar Stepper Remote Control through WebSockets

![Picture](system.jpg)

## E13+.2 Prezentare generala, scop ##

* Scopul este obtinerea unui sistem interactiv care sa permita atat controlul paralel asupra unui motor pas cu pas (stepper motor) unipolar, dar si sa puna in evidenta avantajele unui astfel de actuator.
* Se exploreaza metode avansate de interactiune in timp real intre mai multi utilizatori care folosesc clienti de browser Web cu suport de HTML5 si Java script si un server Apache2 configurat cu suport de WebSocket si extins printr-o aplicatie server de WebSocket realizata in Python3.

## E13+.3 Resurse ##

* Platforma Raspberry Pi (se demonstreaza cu RPi3, dar solutia este compatibila cu oricare dintre variante)
* [Kit de motor pas-cu-pas si driver](https://www.tme.eu/ro/details/oky3199/motoare-pas-cu-pas/okystar/)
	* Motor pas-cu-pas unipolar [28BYJ-48](https://www.tme.eu/Document/0c77683e3b13ecf365460a5c70431942/28BYJ_datasheet.pdf) (DC 5V, 64 pasi electrici cu reductie mecanica 64:1 rezultand 4096 pasi pe rotatie)
	* Driver de motor pas-cu-pas SBT0811 cu [ULN2003AN High-Voltage,High-CurrentDarlingtonTransistorArray](http://www.ti.com/lit/ds/symlink/uln2003a.pdf) cu leduri indicatoare
* Alimentator 5V curent continuu pentru motor
* Camera video (sau web) USB cu suport nativ pentru [USB Video Class](https://en.wikipedia.org/wiki/USB_video_device_class)
* Fire de legatura mama-mama
* Software: [raspbian](https://www.raspberrypi.org/downloads/raspbian/), apache2, python3, [fswebcam](https://github.com/tudorp/fswebcam)
* Pagina de control demonstrativa: [http://smlab.asq.ro](http://smlab.asq.ro)
* Optional: poarta optica pentru stabilire reper de pozitionare

## E13+.4 Surse si programe prototip ##

Aplicatia a fost dezvoltata pornind de la documentatia [HTML5 WebSocket Tutorial](https://websockets.readthedocs.io/en/stable/intro.html#basic-example) si apoi extinsa si adptata pentru contextul de RaspberryPi avand in vedere si pastrarea compatibilitatii cu servicii de publicare in Internet de tipul [Pagekite](http://pagekite.net).

* [Aplicatie Server WebSocket dezvoltata in Python cu asyncio, threading si websocket](https://bitbucket.org/tudor-p/rpiwsstepper/src/master/wss.py)

* [Sursa pagina web principala desvoltata in HTML5 cu javascript](https://bitbucket.org/tudor-p/rpiwsstepper/src/master/index.html)

* [Script de pornire si auto repornire pentru scriptul cu rol WebSocket server](https://bitbucket.org/tudor-p/rpiwsstepper/src/master/wss_startup.sh)

## E13+.5 Mod de desfasurare si urmarire experiment ##

* Proiectul necesita instalarea serverului apache2 pe raspberry pi

### E13+.5.0 Actualizare sistem ###
```
#!bash
sudo apt update
sudo apt upgrade
sudo apt update
```

### E13+.5.1 Instalare git ###
```
#!bash
sudo apt install git
```

### E13+.5.2 Instalare Server Apache ###
```
#!bash
sudo apt install apache2
```

### E13+.5.3 Configurare Server Apache ###

Schimbare drepturi de access la directoarele de continut web
```
#!bash
sudo chown -R pi:www-data /var/www/html/
sudo chmod -R 770 /var/www/html/
```

#### E13+.5.3.1 Instalare suport pentru tunelare WebSocket astfel incat toate conexiunile sa functioneze pe un singur port (e.g. port 80 pentru HTTP si WS) ####
```
#!bash
sudo a2enmod proxy proxy_http proxy_wstunnel
sudo service apache2 restart
```

#### E13+.5.3.2 Editare configurare server Apache pentru redirectarea traficului de protocol WebSocket de pe portul 80 spre portul serverului Python pt WebSocket ###

Pentru a expune un singur port catre internet este necesara configurarea server host-ului de apache conform [documentatiei](https://httpd.apache.org/docs/2.4/mod/mod_proxy_wstunnel.html):
```
#!bash
sudo nano /etc/apache2/sites-enabled/000-default.conf
```
Denumirea fisierului este cea implicita, dar daca ati particularizat instalarea de apache poate sa difere!
Trebuie adaugate urmatoarele doua linii in fisierul 000-default.conf inaintea liniei </VirtualHost>:

```
#!apache.site.config
ProxyRequests off             
ProxyPass /socket.io ws://localhost:3001/socket.io
```
Prima linie inchide o potentiala problema de securitate prin oprirea serviciilor de proxy instalate anterior de la a fi folosite pentru redirectionarea traficului spre Internet.
A doua linie este esentiala pentru redirectarea prin portul HTTP a traficului de WebSocket, care in mod natural ar avea un port separat. Continutul URL-rilor sunt strans legate de continutul fisierelor index.html si wss.py!

Dupa operarea editarii este necesara repornirea serviciului Apache.
```
#!bash
sudo service apache2 restart
```

### E13+.5.4 Testarea compatibilitatii camerei video (web/dvr auto) ###
```
#!bash
lsusb -t | grep Class=Video # Se ruleaza dupa atasarea camerei
```
Ar trebui sa se obtina macar o linie cu textul *Class=Video*, altfel probabil camera nu este compatibila, dar totusi se poate incerca accesarea ei cu aplicatia fswebcam

Listarea detaliata a caracteristicilor camerei se poate face cu:
```
#!bash
lsusb -v | less
```
Iesirea poate contine indicii despre modurile de functionare si rezolutiile suportate.

O alta modalitate de verificare este inspectarea comenzii:
```
#!bash
ls /dev/video*
```
Ar trebui sa afiseze o lista de dispozitive video suplimentare in comparatie cu momentul in care nu este conectata camera.

### E13+.5.5.A Instalarea aplicatiei fswebcam (din repository standard) pentru preluare imagini de la camera web (prima varianta, cu salvare imagini in RAM) ###

```
#!bash
sudo apt install fswebcam # instalare din repositoriul debian/ubuntu (versiune veche, ii lipseste outputul direct in stdout)

sudo mkdir /var/www/html/imag # director pentru montarea discului virtual
sudo echo "tmpfs /var/www/html/imag tmpfs nodev,nosuid,size=1024k 0 0" >> /etc/fstab # pregatire montare automata la pornire
sudo mount -a # montarea discului virtual (doar la prima utilizare pentru ca fstab ruleaza doar la boot)
df -h # verificarea montarii discului

fswebcam -r 320x240 /var/www/html/imag/image.jpg # exemplu de achizitie imagine cu rezolutie specificata
```

### E13+.5.5.B Instalarea aplicatiei fswebcam (cea mai recenta varianta, de pe github) (a doua varianta, nu necesita disc virtual) ###
```
#!bash
sudo apt install build-essential libgd-dev # dependinte necesara pt compilare
cd ~
mkdir dev
cd dev
git clone https://github.com/fsphil/fswebcam.git
cd fswebcam
./configure --prefix=/usr # verifica existenta tuturor dependintelor
make # build local
sudo make install # instalare (poate inlocui variata din repo standard, daca a fost deja instalata cu *apt install*)
```

### E13+.5.5.C Instalarea versiunii aplicatiei fswebcam (de pe github) optimizata pentru viteza de captura (a treia varianta, permite optiunea de rulare --just-dump) ###

Aceeasi pasi ca in sectiunea anterioara doar ca se face clonare din sursa diferita:
```
#!bash
git clone https://github.com/tudorp/fswebcam.git
```

### E13+.5.6 Creare user separat pentru rularea serverului de WebSocket ###

Din motive de securitate o masura minimala de protectie este ca scriptul [wss.py](https://bitbucket.org/tudor-p/rpiwsstepper/src/master/wss.py) sa fie rulat cu drepturi minimale pentru ca orice bresa de securitate posibil introdusa de aceasta, sa ofere posibilitati restranse de exploatare.

In acest sens este necesar crearea unui cont separat (i.e. wss) cu drepturi reduse (membru doar in grupurile gpio si video).

```
#!bash
sudo useradd -G gpio video -m wss # create the new user as a member of gpio and video groups
```

### E13+.5.7 Montarea fisierelor ###

Pentru instalarea aplicatiei este necesara montarea fisierelor proiectului in locatii potrivite, asa cum sugereaza comenzile urmatoare:
```
#!bash
sudo wget https://bitbucket.org/tudor-p/rpiwsstepper/src/master/index.html -O /var/www/html/index.html # installing the html code in the apache web server default content location
sudo chown pi:www-data /var/www/html/index.html # ensure access for pi user and www-data group
sudo apt install python3-dev python3-rpi.gpio python3-websockets python3-pip # ensuring python 3 dependencies are present
sudo su wss # switch shell context to the wss user
wget https://bitbucket.org/tudor-p/rpiwsstepper/src/master/wss.py -O /home/wss/wss.py # install the python websocket server script
wget https://bitbucket.org/tudor-p/rpiwsstepper/src/master/wss_startup.sh -O /home/wss/wss_startup.sh # install the startup manager script
chmod +x /home/wss/wss.py # ensure execute privileges
chmod +x /home/wss/wss_startup.sh
exit # exit wss user shell context

```

### E13+.5.8 Programarea pornirii automate a server-ului de WebSocket ###
Pentru a programa pornirea automata a scriptului */home/wss/wss.py* pot fi abordate mai multe [metode](https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/).

Una dintre cele mai simple variante este adaugarea comenzii de mai jos imediat inaintea ultimei linii (care de regula este *exit 0*) din fisierul sistem */etc/rc.local* folosind editorul preferat (e.g. *sudo nano /etc/rc.local*):
```
#!bash
sudo su wss bash -c /home/wss/wss_startup.sh & # change context to the limitted wss user and launch the websocket server manager script
```
In comanda de mai sus este esentiala lansarea ca proces independent a comenzii prin folosirea operatorului &, altfel pornirea sistemului poate fi afectata.

Suplimentar, este foarte probabil necesara si activarea serviciului *rc.local-service* care lanseaza fisierul in executie */etc/rc.local* la pornirea sistemului:

```
#!bash
sudo systemctl enable rc-local.service
```

Pentru restartarea manuala a serviciului rc.local-service (pentru invocarea la alt moment decat automat la pornirea sistemului):

```
#!bash
sudo systemctl restart rc-local.service
```

Pentru verificarea starii serviciului dupa repornirea sistemului:
```
#!bash
ps -A | grep wss
systemctl status rc-local.service
```

Pentru oprirea scriptului poate fi necesara executia comenzilor (in vederea rularii manuale sau a repornirii):
```
#!bash
sudo killall wss_startup.sh
sudo killall wss.py
```

### E13+.5.9 Conectarea modulelor ###

* Se analizeaza cu atentie datasheet-ul driver-ului in vederea stabilirii tensiunii de alimentare si a polaritatii
* Se asigura masa comuna intre driver si platforma Raspberry Pi
* Se conecteaza intrarile IN1, IN2, IN3 si IN4 ai driver-ului la pinii GPIO 11, 13, 15 si respectiv 16 (ordinea este foarte importanta pentru functionarea corecta)
* Se conecteaza camera video pe usb
* Se conecteaza reteaua
* Se alimenteaza raspberry Pi
* Se alimenteaza driverul, recomandabil, din sursa separata, pentru punerea in functiunea a motorului pas cu pas

### E13+.5.10 Utilizarea paginii demonstrative ###
* Se acceseaza la adresa [smlab.asq.ro](http://smlab.asq.ro)
* Daca scriptul wss.py ruleaza corect, in browser ar trebui sa se raporteaze existenta unui numar de conexiuni
* Utilizarea butoanelor +/- modifica cu un singur pas pozitia rotorului (fapt insesizabil datorita raportului de 1:4096)
* Comanda curenta pe bobinele motorului este vizibila pe ledurile driverului pentru aproximativ 5 secunde, dupa care driver-ul este trecut in stand-by pentru a economisi energie si a proteja motorul
* Utilizarea butoanelor ++/-- modifica pozitia de referinta (tinta) cu un numar mai mare de unitati (e.g. 100) si se poate observa cum rotorul porneste sa urmareasca referinta.
* Timpul de tranzitie este controlat de perioada de asteptare intre doi pasi (a se analiza functia *stepper_management()* din [wss.py](https://bitbucket.org/tudor-p/rpiwsstepper/src/master/wss.py)

## E13+.6 Probleme propuse ##

* Extinderea interfetei Web si a suportului de backend pentru a putea regla viteza de avans (perioada) dintre pasi
* Extinderea cu suport pentru pozitionarea rotorului la puncte absolute (nu doar miscari relative)
* Extinderea cu suport pentru controlul accelerarii si decelerarii rotorului (pornire si oprire lina)
* Extinderea cu suport de autentificare pentru a putea lansa comenzi de miscare
* Extinderea cu suport de protectie impotriva abuzului de exploatare (limitare numar de cereri servite, oprire acces)
* Extinderea cu un senzor de pozitie (e.g. poarta optica sau buton de capat (endstop switch)) pentru calibrarea pozitionarii absolute

## E13+.7 Experimentul poate fi extins pentru

* Controlul precis al unui dispozitiv (e.g. seringa/injectomat)
* Reglajul automat al acceleratiei unui motor termic
* Calibrarea pozitiei rotorului motorului cu ajutorul camerei video
* Reglarea automata a iluminarii naturale cu ajutorul jaluzelelor
* Reglarea unui proces chimic periculos

## E13+.8 Bibliografie ##
[HTML5 WebSocket Tutorial](https://websockets.readthedocs.io/en/stable/intro.html#basic-example)

[Apache WebSocket Redirect](https://httpd.apache.org/docs/2.4/mod/mod_proxy_wstunnel.html):

[Motor Theory](https://www.youtube.com/watch?v=Ew6eVGnj7r0)

[More Theory](https://www.youtube.com/watch?v=VMwv4XFZ2L0)

[Technical Video about the Used Stepper](https://www.youtube.com/watch?v=Sl2mzXfTwCs)

[Unipolar to Bipolar Stepper Conversion](https://www.youtube.com/watch?v=ROFjf_7IrRA)

[Stepper Motor Tutorial](https://www.geeetech.com/wiki/index.php/Stepper_Motor_5V_4-Phase_5-Wire_%26_ULN2003_Driver_Board_for_Arduino)

[Stepper Motor Driver](http://eeshop.unl.edu/pdf/Stepper+Driver.pdf)

[ULN2003AN High-Voltage,High-CurrentDarlingtonTransistorArray](http://www.ti.com/lit/ds/symlink/uln2003a.pdf) 

[28BYJ-48 Stepper Motor Datasheet](https://www.tme.eu/Document/0c77683e3b13ecf365460a5c70431942/28BYJ_datasheet.pdf)

[28BYJ-48 Tutorial](https://www.seeedstudio.com/blog/2019/03/04/driving-a-28byj-48-stepper-motor-with-a-uln2003-driver-board-and-arduino/)

[USB Video Class](https://en.wikipedia.org/wiki/USB_video_device_class)

[fswebcam maintainer website](https://github.com/fsphil/fswebcam.git)

[Autostart programs on Raspberry Pi](https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/)

[Programare Asincrona in Python](https://realpython.com/async-io-python/)

### (c) 2020 Tudor P. ###
