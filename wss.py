#!/usr/bin/python3

# (c)2020 Tudor P
# Stepper motor controller
# https://github.com/mortzdk/Websocket
# https://websockets.readthedocs.io/en/stable/intro.html
# WS server example that synchronizes state across clients

import asyncio
import json
import logging
import websockets
import subprocess
import base64
import RPi.GPIO as GPIO
import threading
import time

logging.basicConfig()

STATE = {"value": 0, "target": 0
        , "period": 0.002
        , "img": 'data:image/jpeg;base64'
        , "refresh": 0.25
        , "lastrefresh": time.time()
        , "lastchange": time.time()
        , "standbytimeout": 5.0
        , } # state information

CMDS = [ [0, 0], ] # on startup set reference to 0

USERS = set() # user list

STEPS = [[1,0,0,0], [1,1,0,0], [0,1,0,0], [0,1,1,0], [0,0,1,0], [0,0,1,1], [0,0,0,1], [1,0,0,1]] 

PHASE_PINS = [11, 13, 15, 16] # Raspberry Pi GPIO pins used for IN1, IN2, IN3, IN4 stepper driver inputs

def stepper_setup():
    # Requires pynthon3 support and Rpi.gpio libraries
    # sudo apt-get install python3-dev python3-rpi.gpio
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    for ph in PHASE_PINS:
        GPIO.setup(ph, GPIO.OUT)
        GPIO.output(ph, 0)
    
def stepper_step():
    GPIO.output(PHASE_PINS, STEPS[STATE['value']&7])
    STATE['lastchange'] = time.time()

def stepper_standby():
    # the stepper get slightly warm when left active
    # powering it down would help save power and protect the windings
    GPIO.output(PHASE_PINS, [0,0,0,0])
    STATE['lastchange'] = time.time()

def stepper_fwd():
    STATE['value'] = (STATE['value'] + 1) #& 4095
    stepper_step()

def stepper_rev():
    STATE['value'] = (STATE['value'] - 1) #& 4095
    stepper_step()

def stepper_management():
    # syncronous calls only on separated thread for better timings
    global CMDS
    while True:
        time.sleep(STATE['period'])
        if len(CMDS)>0:
            if CMDS[0][0] == 0:
                STATE['target'] = CMDS[0][1]
            else:
                STATE['target'] = (STATE['target'] + CMDS[0][1]) #& 4095
            CMDS = CMDS[1:]

        if STATE['target'] < STATE['value']:
            stepper_rev()
        elif STATE['target'] > STATE['value']:
            stepper_fwd()
        if time.time() - STATE['lastchange'] > STATE['standbytimeout']:
            stepper_standby()

async def comm_management():
    # async implementation for best-effort, non-critical timing
    while True:
        await asyncio.sleep(STATE['period'])
        if time.time() - STATE['lastrefresh'] > STATE['refresh'] and STATE['lastchange'] > STATE['lastrefresh']:
            STATE["lastrefresh"] = time.time()
            STATE["img"] = await capture_with_fswebcam()
            await notify_state()
    
# https://stackoverflow.com/questions/4760215/running-shell-command-and-capturing-the-output
async def capture_with_fswebcam(width=320, height=240, dump_to_stdout=True, just_dump=True):
    # dump frame to stdout (-) actualy works only on latest official version available on github
    # https://github.com/fsphil/fswebcam
    cmd = ['fswebcam', '-q', '-r', str(width)+'x'+str(height), '--dumpframe', '-']
    if just_dump:
        # requires a fix and a customization compared to latest official version!
        # bitbucket.org/tudor-p/fswebcam
        cmd.append('--just-dump')
    image = subprocess.run(cmd, stdout=subprocess.PIPE).stdout
    return 'data:image/jpeg;base64,' + base64.b64encode(image).decode('ascii')

def state_event():
    return json.dumps({"type": "state", **STATE})

def users_event():
    return json.dumps({"type": "users", "count": len(USERS)})

async def try_send(websocket, message):
    try:
        await websocket.send(message)
    except:
        # some connections are expired/closed when are attemted to be served
        pass

async def notify_state():
    if USERS:  # asyncio.wait doesn't accept an empty list
        message = state_event()
        await asyncio.wait([try_send(user, message) for user in USERS])

async def notify_users():
    if USERS:  # asyncio.wait doesn't accept an empty list
        message = users_event()
        await asyncio.wait([try_send(user, message) for user in USERS])

async def register(websocket):
    USERS.add(websocket)
    await notify_users()

async def unregister(websocket):
    USERS.remove(websocket)
    await notify_users()

# method called on any new established websocket (handles each socket lifecycle)
async def counter(websocket, path):
    # register(websocket) sends user_event() to all clients
    await register(websocket)
    try:
        await websocket.send(state_event()) # initialize new client remote state
        async for message in websocket: # handle received websocket messages
            data = json.loads(message)
            if data["action"] == "minus":
                CMDS.append([1,-1])
                #await notify_state()
            elif data["action"] == "plus":
                CMDS.append([1,1])
                #await notify_state()
            elif data["action"] == "rev":
                CMDS.append([1,-128])
            elif data["action"] == "fwd":
                CMDS.append([1,128])
            else:
                logging.error("unsupported event: {}", data)
    finally:
        await unregister(websocket)

stepper_setup()

threading.Thread(target=stepper_management).start()
asyncio.get_event_loop().create_task(comm_management())

start_server = websockets.serve(counter, "localhost", 3001)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

